import {
    USERS_MANAGER_SET_USERS,
    USERS_MANAGER_SET_SELECTED_USER
} from "../actions";

const initialState = {
    users: [],
    selectedUserId: "",
    isLoading: false,
    responseError: null
};

const usersManager = (state = initialState, { type, payload }) => {
    switch (type) {
        case USERS_MANAGER_SET_USERS:
            return {
                ...state,
                users: payload
            };

        case USERS_MANAGER_SET_SELECTED_USER:
            return {
                ...state,
                selectedUserId: payload
            }

        default:
            return state;
    }
}

export default usersManager;
